#' @param scriptvars [\code{list}]\cr
#'   Named list of script variables set via the Cornerstone "Script Variables" menu.
#'   For details see below.
