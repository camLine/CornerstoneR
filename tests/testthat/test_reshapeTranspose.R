test_that("Transpose", {
  env = globalenv()
  
  # default settings for script variables
  scriptvars = list(convert.numeric = TRUE)
  
  # standard
  dtTest = data.table(data1 = rnorm(5), data2 = runif(5))
  createCSEnvir(dtTest
                , lstScriptVars = scriptvars
                , env = env
  )
  # init cs.* functions (only necessary for local call)
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_data_table(res$reshapeTranspose, nrows = 2, ncols = 6, col.names = "named")
  expect_data_table(res$reshapeTranspose[, -1], types = "numeric")
  expect_equal(colnames(res$reshapeTranspose), c("colnames", paste0("V", 1:5)))
  
  # handling missing names and duplicated names on header column
  dtTest$names <- rep(NA,5)
  createCSEnvir(dtTest
                , strGroups = "names"
                , lstScriptVars = scriptvars
                , env = env
  )
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_equal(colnames(res$reshapeTranspose), c("colnames", paste0("Variable_", 1:5)))
  
  # Only one duplicated name
  dtTest$names <- rep("Name",5)
  createCSEnvir(dtTest
                , strGroups = "names"
                , lstScriptVars = scriptvars
                , env = env
  )
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_equal(colnames(res$reshapeTranspose), c("colnames", paste0("Name_", 1:5)))
  
  # 2 duplicated names
  dtTest$names <- c("name", "name", "abc", "this", "this")
  createCSEnvir(dtTest
                , strGroups = "names"
                , lstScriptVars = scriptvars
                , env = env
  )
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_equal(colnames(res$reshapeTranspose), c("colnames", "name_1", 
                                                 "name_2", "abc", "this_1", "this_2"))
  
  # Only one duplicated name
  dtTest$names <- rep("Name",5)
  createCSEnvir(dtTest
                , strGroups = "names"
                , lstScriptVars = scriptvars
                , env = env
  )
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_equal(colnames(res$reshapeTranspose), c("colnames", paste0("Name_", 1:5)))
  
  # mixed, convert.numeric = FALSE
  scriptvars1 = scriptvars
  scriptvars1$convert.numeric = FALSE
  dtTest = data.table(data1 = rnorm(5), data2 = sample(LETTERS, 5))
  createCSEnvir(dtTest
                , lstScriptVars = scriptvars1
                , env = env
  )
  # init cs.* functions (only necessary for local call)
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_data_table(res$reshapeTranspose, types = "character", nrows = 2, ncols = 6, col.names = "named")
  expect_equal(colnames(res$reshapeTranspose), c("colnames", paste0("V", 1:5)))
  
  # mixed, convert.numeric = TRUE
  dtTest = data.table(data1 = rnorm(5), data2 = sample(LETTERS, 5))
  createCSEnvir(dtTest
                , lstScriptVars = scriptvars
                , env = env
  )
  # init cs.* functions (only necessary for local call)
  createCSFunctions(env = env)
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_data_table(res$reshapeTranspose, nrows = 2, ncols = 6, col.names = "named")
  expect_data_table(res$reshapeTranspose[, -1], types = "numeric")
  
  # column with data for new variable names
  # generate data
  dtTest = data.table(varnames = sample(LETTERS, size = 5), data1 = rnorm(5), data2 = runif(5))
  createCSEnvir(dtTest
                , strGroups = "varnames"
                , lstScriptVars = scriptvars
                , env = env
  )
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_data_table(res$reshapeTranspose, nrows = 2, ncols = 6, col.names = "named")
  expect_equal(colnames(res$reshapeTranspose), c("colnames", dtTest[[1]]))
  
  # input is numeric as string and should be returned as numeric
  dtTest = data.table(data1 = c("1,2", "2,3", "3,4"), data2 = c("9,87", "7,65", "5,43"))
  createCSEnvir(dtTest
                , lstScriptVars = scriptvars
                , env = env
  )
  expect_true(reshapeTranspose())
  res = reshapeTranspose(return.results = TRUE)
  expect_data_table(res$reshapeTranspose, nrows = 2, ncols = 4, col.names = "named")
  expect_data_table(res$reshapeTranspose[, -1], types = "numeric")
})