test_that("CastWide", {
  env = globalenv()
  
  # default settings for script variables
  scriptvars = list(drop = TRUE, simpleName = FALSE, aggr.fun = "mean")
  
  # single
  createCSEnvir(Indometh
                , strPreds = colnames(Indometh)[2], strResps = colnames(Indometh)[3]
                , strGroups = colnames(Indometh)[1]
                , lstScriptVars = scriptvars
                , env = env
  )
  # init cs.* functions (only necessary for local call)
  createCSFunctions(env = env)
  expect_true(reshapeWide())
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 11, ncols = 7)
  expect_equal(colnames(res$reshapeWide)[c(1, 2, 7)], c("time", "conc_mean_1", "conc_mean_3"))
  expect_equal(res$reshapeWide$conc_mean_1, head(Indometh$conc, 11))
  expect_equal(res$reshapeWide$conc_mean_6, tail(Indometh$conc, 11))
  
  # missing aggregation function
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = ""
  createCSEnvir(Indometh
                , strPreds = colnames(Indometh)[2], strResps = colnames(Indometh)[3]
                , strGroups = colnames(Indometh)[1]
                , lstScriptVars = scriptvars1
                , env = env
  )
  # init cs.* functions (only necessary for local call)
  createCSFunctions(env = env)
  expect_true(reshapeWide())
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 11, ncols = 7)
  expect_equal(colnames(res$reshapeWide)[c(1, 2, 7)], c("time", "conc_first_1", "conc_first_3"))
  expect_equal(res$reshapeWide$conc_first_1, head(Indometh$conc, 11))
  expect_equal(res$reshapeWide$conc_first_6, tail(Indometh$conc, 11))
  
  # simple Name TRUE
  scriptvars2 = scriptvars
  scriptvars2$simpleName <- TRUE
  createCSEnvir(Indometh
                , strPreds = colnames(Indometh)[2], strResps = colnames(Indometh)[3]
                , strGroups = colnames(Indometh)[1]
                , lstScriptVars = scriptvars2
                , env = env
  )
  # init cs.* functions (only necessary for local call)
  createCSFunctions(env = env)
  expect_true(reshapeWide())
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 11, ncols = 7)
  expect_equal(colnames(res$reshapeWide)[c(1, 2, 7)], c("time", "1", "3"))
  expect_equal(res$reshapeWide$`1`, head(Indometh$conc, 11))
  expect_equal(res$reshapeWide$`6`, tail(Indometh$conc, 11))
  
  # additional response
  IndoExt = cbind(Indometh, rnd = rnorm(66))
  createCSEnvir(IndoExt
                , strPreds = colnames(IndoExt)[2], strResps = colnames(IndoExt)[3:4]
                , strGroups = colnames(IndoExt)[1]
                , lstScriptVars = scriptvars
                , env = env
  )
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 11, ncols = 13)
  expect_equal(colnames(res$reshapeWide)[c(1, 2, 7, 8, 13)]
               , c("time", "conc_mean_1", "conc_mean_3", "rnd_mean_1", "rnd_mean_3")
  )
  
  # multiple groups
  DT = data.table(v1 = rep(1:2, each = 6), v2 = rep(rep(1:3, 2), each = 2), v3 = rep(1:2, 6), v4 = rnorm(6))
  createCSEnvir(DT
                , strPreds = "v2", strResps = "v4"
                , strGroups = c("v1", "v3")
                , lstScriptVars = scriptvars
                , env = env
  )
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 3, ncols = 5)
  expect_equal(colnames(res$reshapeWide)
               , c("v2", "v4_mean_1_1", "v4_mean_1_2", "v4_mean_2_1", "v4_mean_2_2")
  )
  
  # multiple predictors
  createCSEnvir(DT
                , strPreds = c("v2", "v3"), strResps = "v4"
                , strGroups = "v1"
                , lstScriptVars = scriptvars
                , env = env
  )
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 6, ncols = 4)
  expect_equal(colnames(res$reshapeWide), c("v2", "v3", "v4_mean_1", "v4_mean_2"))
  
  # aggreation function working
  # carstats standard
  createCSEnvir(carstats[, .(MPG, Cylinders, Model.Year)]
                , strPreds = "Model.Year", strResps = "MPG", strGroups = "Cylinders"
                , lstScriptVars = scriptvars
                , env = env
  )
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 13, ncols = 6, type = "numeric")
  # carstats more functions
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = "mean, sd"
  createCSEnvir(carstats[, .(MPG, Cylinders, Model.Year)]
                , strPreds = "Model.Year", strResps = "MPG", strGroups = "Cylinders"
                , lstScriptVars = scriptvars1
                , env = env
  )
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 13, ncols = 11, type = "numeric")
  # carstats wrong function
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = "mean, notdefinedfunction"
  createCSEnvir(carstats[, .(MPG, Cylinders, Model.Year)]
                , strPreds = "Model.Year", strResps = "MPG", strGroups = "Cylinders"
                , lstScriptVars = scriptvars1
                , env = env
  )
  expect_error(reshapeWide(), "notdefinedfunction")
  # carstats aggregation by other column, check assertion
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = "mean, minby(Weight), sd, maxby(Blup)"
  createCSEnvir(carstats[, .(Origin, Displacement, Cylinders, Weight)]
                , strPreds = "Origin", strResps = "Displacement", strGroups = "Cylinders", strAuxs = "Weight"
                , lstScriptVars = scriptvars1
                , env = env
  )
  expect_error(reshapeWide())
  # carstats aggregation by other column
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = "mean, minby(Weight), sd, maxby(Weight)"
  createCSEnvir(carstats[, .(Origin, Displacement, Cylinders, Weight)]
                , strPreds = "Origin", strResps = "Displacement", strGroups = "Cylinders", strAuxs = "Weight"
                , lstScriptVars = scriptvars1
                , env = env
  )
  expect_true(reshapeWide())
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 7, ncols = 5*4+1)
  # carstats aggregation by multiple response columns
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = "mean, minby(Weight), sd, maxby(Weight)"
  createCSEnvir(carstats[, .(Origin, Displacement, Horsepower, Cylinders, Weight)]
                , strPreds = "Origin", strResps = c("Displacement", "Horsepower")
                , strGroups = "Cylinders", strAuxs = "Weight"
                , lstScriptVars = scriptvars1
                , env = env
  )
  expect_true(reshapeWide())
  res = reshapeWide(return.results = TRUE)
  expect_data_table(res$reshapeWide, nrows = 7, ncols = 5*4*2+1)
  # carstats, check results from minby and maxby
  scriptvars1 = scriptvars
  scriptvars1$aggr.fun = "minby(Weight), maxby(Weight)"
  createCSEnvir(carstats[, .(Origin, Displacement, Cylinders, Weight)]
                , strPreds = "Origin", strResps = "Displacement", strGroups = "Cylinders", strAuxs = "Weight"
                , lstScriptVars = scriptvars1
                , env = env
  )
  expect_true(reshapeWide())
  res = reshapeWide(return.results = TRUE)
  # manually proven results
  expect_equal(res$reshapeWide$Displacement_minby_4, c(122, 79, 97, 68, 72, 104, 98))
  expect_equal(res$reshapeWide$Displacement_maxby_4, c(122, 120, 146, 107, 134, 130, 151))
})