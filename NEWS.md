# CornerstoneR 3.3.0

-   Add function 'Validation Scores' for Model Evaluation.
-   Update 'Gaussian Process Regression': Improved default starting values for bandwidth hyperparameters.
-   Update 'Reliability Distribution Fitting' Graphs: add script variable for number of points to be plotted, set x-axis labels as variable names and output plotted data.
-   Fixed 'Reliability Distribution Fitting': no output of Hazard Plot in case of negative values.
-   Fixed graph labels for matrix graph outputs (requires Cornerstone 8.0.0.3).
-   Fixed error handling in 'Decision Tree' and 'Random Forest' in case of failing model fit.
-   Removed package 'rcompanion' dependency.
-   Added file 'showRDialogs.txt' to show only necessary Cornerstone dialogs in CEDA.

# CornerstoneR 3.2.0

-   Add function 'k-Means Clustering'.
-   Add function 'Moving Average Filter' for different types of time series smoothing.
-   Add function 'Time Series Models' for time series forecasting.
-   Replace function 'Survival Analysis' with 'Reliability Distribution Fitting'.
-   Add extensive list of distributions to be supported in 'Reliability Distribution Fitting'.
-   Update 'Fit Function' output.
-   Update 'Correlation Analysis' with confidence intervals.
-   Update 'Autocorrelation' lag graphs.
-   Update 'Transpose' to handle missing values in header.
-   Update 'Logistic Regression' to allow for multiple responses, and use only brushed rows.
-   Allow 'Model Predict' function to predict for Logistic Regression Models.
-   Add option to 'Missing Value Handling' to keep only variables with x% of complete cases.
-   Fixed various bugs.

# CornerstoneR 3.1.0

-   Add function 'Gaussian Process Regression' for a nonparametric, nonlinear Bayesian approach to regression.
-   Expand function 'Random Forest Prediction' to 'Model Prediction' for prediction of model building methods Gaussian Process Regression, Random Forest and Decision Tree.
-   Add function 'Autocorrelation' for (partial) auto- and cross-correlation.
-   Add function 'Time Series Feature Extraction' for feature detection in time series data.
-   Add function 'Correlation Analysis' for correlation analysis of variables.
-   Update 'Missing Value Handling' with linear and cubic interpolation.
-   Update 'Decision Tree' with graphic size options.
-   Fixed various bugs.
-   Support R up to version 4.1.3.

# CornerstoneR 3.0.0

-   Add function 'Decision Tree' for classification and regression.
-   Add function 'Logistic Regression' for logistic model fitting.
-   Add function 'Survival Analysis' for Weibull and Lognormal distribution fitting.
-   Add function 'Missing Values Handling' for missing value detection and imputation.
-   Rename 'Fit Function via non-linear regression' to 'Fit Function'.
-   Update 'Fit Function' with new functions and modifications.
-   Add new graph and computed column interface to Cornerstone.
-   Re-implement "Mosaic Plot" using ggmosaic package.
-   Fixed various bugs.
-   Support R up to version 4.0.5.

# CornerstoneR 2.0.2

-   Fixed various bugs
-   Update maintainer

# CornerstoneR 2.0.1

-   Add function 'showVersions' to display R and CornerstoneR versions.
-   Fix 'reshapeWide' aggregation functions.

# CornerstoneR 2.0.0

-   Add vignettes for all functions.
-   Add function 'matchNearestNeighbor'.
-   Add function 'redirectDataset'.
-   Add function 'reshapeTranspose'.
-   Update 'mosaicPlot' to multi response and return of long contingency table.
-   Update 'fitFunction' to minpack.lm with its Levenberg-Marquardt algorithm.
-   Update 'fitFunction' to multiple groups.
-   Update 'reshapeLong' UI for more intuitive behavior (switch predictors and groups).
-   Update 'reshapeWide' with aggregation functions.
-   Fixed various bugs

# CornerstoneR 1.1.1

-   Cleanup DESCRIPTION

# CornerstoneR 1.1.0

-   Random Forest: Add importance mode and respect unordered factors to script variables.
-   Random Forest: Export split rule to statistics.
-   Random Forest: Add function 'randomForestPredict' to predict new data via a saved forest.
-   Fit Function via nls: User defined functions are now possible via script variables.
-   'FitFunction', 'mosaicPlot', 'ReshapeLongWide': Add three dots to function call.

# CornerstoneR 1.0.1

-   Fix tests.

# CornerstoneR 1.0.0

-   Initial CRAN release.
