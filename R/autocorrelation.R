#' @title Auto- and Cross-Correlation
#' @description
#' Calculate autocorrelation and partial autocorrelation to help identify 
#' the time series model.
#' In addition this function can also be used to calculate cross-correlation.
#' @template dataset
#' @template predictors
#' @template responses
#' @template scriptvars
#' @template returnResults
#' @export
#' @details
#' If only predictors or responses are given, calculate auto- and partial 
#' autocorrelation.
#' For calculating cross correlation, pass both predictors and responses and 
#' check the cross-correlation box in the script variables. \cr
#'   Four script variables are summarized in \code{scriptvars} list:\cr
#'   \describe{
#'     \item{max.lag}{[\code{integer(1)}]\cr
#'       The preferred time delay used in the calculation. 
#'       Default is \code{12}.}
#'     \item{conf.level}{[\code{integer(1)}]\cr
#'       The confidence Level.
#'       It can be set to \code{0.90}, \code{0.95}, \code{0.99} or \code{0.999}.
#'       Default is \code{0.90}.}
#'     \item{cross.cor}{[\code{logical(1)}]\cr
#'       If to calculate cross-correlation between predictors and responses.
#'       Default is \code{FALSE}.}
#'     \item{out.LagTab}{[\code{logical(1)}]\cr
#'       If to output lag table. Default is \code{FALSE}.}
#'       }
#' @return
#'   Logical [\code{TRUE}] invisibly and outputs to Cornerstone or,
#'   if \code{return.results = TRUE}, \code{\link{list}} of
#'   resulting \code{\link{list}} objects:
#'   \item{autoRes}{
#'   contains two \code{\link{data.frame}}
#'   autocorrelation summary table and partial autocorrelation summary table.
#'   }
#'   \item{crossRes}{
#'   contains a \code{\link{data.frame}} with all cross-correlation in a summary
#'   table.
#'   }
#'   \item{lagTable}{
#'   contains Lag Table \code{\link{data.frame}} for each variable.
#'   }
#' @examples
#' airquality <- na.omit(airquality)
#' autocorrelation(airquality,
#'   preds = names(airquality)[2:3],
#'   resps = names(airquality)[4:5],
#'   scriptvars = list(max.lag = 5, conf.level = "0.90", out.LagTab = FALSE, 
#'                     cross.corr = TRUE),
#'   return.results = TRUE
#' )
autocorrelation <- function(dataset = cs.in.dataset(),
                            preds = cs.in.predictors(),
                            resps = cs.in.responses(),
                            scriptvars = cs.in.scriptvars(),
                            return.results = FALSE) {
  dataset <- as.data.frame(dataset)

  assertCharacter(preds, any.missing = FALSE)
  assertCharacter(resps, any.missing = FALSE)

  x <- dataset[, preds, drop = FALSE]
  y <- dataset[, resps, drop = FALSE]
  assertDataFrame(x, any.missing = FALSE, types = "numeric")
  assertDataFrame(y, any.missing = FALSE, types = "numeric")

  assertList(scriptvars, len = 4)
  assertCount(scriptvars$max.lag, positive = TRUE)
  assertChoice(scriptvars$conf.level, c("0.90", "0.95", "0.99", "0.999"))
  assertFlag(scriptvars$cross.corr)
  assertFlag(scriptvars$out.LagTab)

  lag <- as.numeric(scriptvars$max.lag)
  conf.level <- as.numeric(scriptvars$conf.level)
  cross.corr <- scriptvars$cross.corr
  out.LagTab <- scriptvars$out.LagTab

  createLagTable <- function(x) {
    dataLag <- list()
    dataZoo <- zoo::zoo(x)

    for (i in seq_len(ncol(dataZoo))) {
      tmp <- colnames(x[i]) # name of the current var
      dataLag[[i]] <- data.frame(merge(dataZoo[, i], 
                                       lag(dataZoo[, i], -(seq_len(lag)))))
      colnames(dataLag[[i]]) <- paste(tmp, "Lag", 0:(ncol(dataLag[[i]]) - 1), 
                                      sep = "_")
      names(dataLag)[i] <- tmp
      cs.out.dataset(dataLag[[i]], name = paste("Lag table for", tmp))
    }
    return(dataLag)
  }

  calculateAutocorr <- function(x, y) {
    # dataTS : time series object
    # dataACF: acf values from acf object
    # dataPACF: pacf values from pacf object
    dataTS <- dataACF <- dataPACF <- list()
    autoDf <- cbind(x, y)
    dataZoo <- zoo::zoo(autoDf) # use zoo to create lag

    for (i in seq_len(ncol(dataZoo))) {
      tmp <- colnames(autoDf[i]) # name of the current pred
      dataTS[[i]] <- as.ts(autoDf[, i])

      tmpACF <- acf(dataTS[[i]], lag.max = lag, plot = FALSE)
      dataACF[[i]] <- data.frame(c(tmpACF$acf))
      names(dataACF[[i]]) <- paste("ACF", tmp, sep = "_")

      tmpPACF <- pacf(dataTS[[i]], lag.max = lag, plot = FALSE)
      dataPACF[[i]] <- data.frame(c(NA, (tmpPACF$acf)))
      names(dataPACF[[i]]) <- paste("PACF", tmp, sep = "_")

      if (var(autoDf[, i]) > 0) {
        cs.out.png(name = paste("ACF/PACF graph for", tmp), 1200, 600)
        acf_pacf <- graphics::par(mfrow = 1:2, cex = 1.8)
        plot(tmpACF, ci = conf.level, ylab = "Autocorrelation", main = NA, 
             ci.type = "ma")
        plot(tmpPACF, ci = conf.level, ylab = "Partial Autocorrelation", 
             main = NA, ci.type = "ma")
      }
    }
    dataACF <- as.data.frame(dataACF)
    dataPACF <- as.data.frame(dataPACF)

    lag <- c(tmpACF$lag)
    dataACF <- cbind(Lag = lag, dataACF)
    dataPACF <- cbind(Lag = lag, dataPACF)

    cs.out.dataset(dataACF, name = "Autocorrelation Summary")
    cs.out.dataset(dataPACF, name = "Partial Autocorrelation Summary")

    autoRes <- list(ACF = dataACF, PACF = dataPACF)
    return(autoRes)
  }

  calculateCrosscorr <- function(x, y) {
    # dataCCF: contains all ccf between diff pairs of (pred, resp)
    dataCCF <- list()

    # create Lag and define nrow of the output
    dataCCF[["Lag"]] <- ccf(x[, 1], y[, 1], lag.max = lag, type = "correlation", 
                            plot = FALSE)$lag

    for (i in seq_len(ncol(x))) {
      for (j in seq_len(ncol(y))) {
        tmpY <- colnames(y[j])
        tmpX <- colnames(x[i])
        if (ncol(x) == 1 & ncol(y) > 1) {
          name <- paste("CCF", tmpY, sep = "_")
        } else if (ncol(x) > 1 & ncol(y) == 1) {
          name <- paste("CCF", tmpX, sep = "_")
        } else if (ncol(x) == 1 & ncol(y) == 1) {
          name <- "CCF"
        } else {
          name <- paste("CCF", tmpX, tmpY, sep = "_")
        }

        tmpCCF <- ccf(x[, i], y[, j], lag.max = lag, type = "correlation", 
                      plot = FALSE)
        dataCCF[[name]] <- tmpCCF$acf
        
        if (var(x[, i]) > 0 & var(y[, j]) > 0) {
          cs.out.png(name = paste("CCF graph for", tmpX, "and", tmpY), 600, 600)
          graphics::par(cex = 1.8)
          plot(tmpCCF, ci = conf.level, ylab = "Cross-Correlation", main = NA)
        }
      }
    }
    dataCCF <- as.data.frame(dataCCF)
    cs.out.dataset(dataCCF, name = "Cross-Correlation Summary")
    return(dataCCF)
  }

  autoRes <- calculateAutocorr(x, y)
  
  cs.out.graph(x = data.frame(Lag = as.factor(autoRes[[1]][,"Lag"])),
               y = autoRes[[1]][, -which(names(autoRes[[1]]) %in% "Lag"), 
                                drop = FALSE],
               name = "Autocorrelation Summary", 
               brush = FALSE, 
               graphtype = "Bar",
               options = "xLabel = Lag, yLabel = Autocorrelation")
  
  newPACF <- autoRes[[2]]
  # if PACF for lag 0 is NA
  if (all(is.na(autoRes[[2]][1, -which(names(autoRes[[2]]) %in% "Lag")]))) {
    newPACF <- autoRes[[2]][-1, ]
  }
  
  cs.out.graph(x = data.frame(Lag = as.factor(newPACF[, "Lag"])),
               y = newPACF[, -which(names(newPACF) %in% "Lag"), drop = FALSE],
               name = "Partial Autocorrelation Summary", 
               brush = FALSE, 
               graphtype = "Bar",
               options = "xLabel = Lag, yLabel = Partial Autocorrelation")
  
  
  if (!cross.corr) {
    assertTRUE(length(x) + length(y) >= 1)
  } else {
    assertTRUE(length(x) >= 1)
    assertTRUE(length(y) >= 1)
    crossRes <- calculateCrosscorr(x, y)
    if (ncol(x) == 1 & ncol(y) > 1) {
      nameGraph <- paste("Cross-Correlation Summary for", colnames(x))
    } else if (ncol(x) > 1 & ncol(y) == 1) {
      nameGraph <-  paste("Cross-Correlation Summary for", colnames(y))
    } else {
      nameGraph <- "Cross-Correlation Summary"
    }
    cs.out.graph(x = data.frame(Lag = as.factor(crossRes[, "Lag"])),
                 y = crossRes[, -which(names(crossRes) %in% "Lag"), 
                              drop = FALSE],
                 name = nameGraph, 
                 brush = FALSE, 
                 graphtype = "Bar",
                 options = "xLabel = Lag, yLabel = Cross-Correlation")
  }
  
  if (out.LagTab) lagTable <- createLagTable(cbind(x, y))

  if (return.results) {
    res <- list(autoRes = autoRes)
    if (cross.corr) res$crossRes <- crossRes
    if (out.LagTab) res$lagTable <- lagTable
    return(res)
  } else {
    invisible(TRUE)
  }
}