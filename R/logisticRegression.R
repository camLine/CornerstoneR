#' @title Fit Logistic Regression
#' @description
#'   Fit a logistic regression model with logit or probit link using the
#'   \code{\link[stats]{glm}} function.
#' @template dataset
#' @template predictors
#' @template responses
#' @template brush
#' @template scriptvars
#' @template returnResults
#' @templateVar packagelink \code{\link[stats]{glm}}
#' @details
#'   The following script variables are summarized in \code{scriptvars} list:\cr
#'   \describe{
#'   \item{use.rows}{[\code{character(1)}]\cr
#'       Rows to use in parameter optimization. Possible values are \code{all},
#'       \code{non-brushed}, or \code{brushed}. Default is \code{all}.}
#'     \item{link}{[\code{character(1)}]\cr
#'       The preferred link function to select from \code{logit} and
#'       \code{probit}. Default is \code{logit}.}
#'     \item{preds.fmla}{[\code{character(1)}]\cr
#'       The type of effects to select from \code{Linear}, \code{Linear+},
#'       \code{Interactions} and \code{Quadratic}.
#'       Default is \code{Linear}.}
#'     \item{significance.level}{[\code{character(1)}]\cr
#'       The significance level (also called "alpha error") for the automatic
#'       stepwise regression.
#'       It can be set to \code{0.10}, \code{0.05}, \code{0.01} or \code{0.001}.
#'       Default is \code{0.10}.}
#'     \item{stepwise}{[\code{logical(1)}]\cr
#'       If TRUE, a full automatic stepwise regression based on the chosen
#'       significance level is fulfilled.
#'       The principles of Model Hierarchy are taken into account. The constant
#'       term cannot be left out of the regression.
#'       May slow down the computation. Default is FALSE.}
#'   }
#' @return
#'   Logical [\code{TRUE}] invisibly and outputs to Cornerstone or,
#'   if \code{return.results = TRUE}, \code{\link{list}} of
#'   resulting \code{\link{data.table}} objects:
#'   \item{regression_table}{
#'   Regression Table including the model terms with
#'   corresponding p values (significance), and the evaluation measures
#'   R Squared, adjusted R Squared, RMS Error and residual degrees of freedom.
#'   Use this table to decide whether to conduct a stepwise regression (see
#'   scriptvars).
#'   }
#'   \item{goodness_of_fit}{
#'   Table with Goodness of Fit measures for each response variable.
#'   Includes the count (number of observations used to compute logistic
#'   regression (only complete cases)), R Squared, adj R Squared, RMS Error and
#'   Residual df.
#'   }
#'   \item{coeff_table}{
#'     Coefficient table for each response. Includes coefficients of the model 
#'     function, standard errors, t values and p values (significance).
#'   }
#'   \item{coeff_cor_matrix}{
#'     Coefficient correlation matrix of the model terms, based on
#'     variance-covariance matrix, for each response. The corresponding 
#'     correlation plot will automatically be plotted.
#'   }
#'   \item{fitted_values}{
#'   Fit estimate table including fitted values, residuals and leverages for
#'   every observation.
#'   }
#'   \item{prediction_table}{
#'   Prediction table. Includes minimum, maximum and mean/mode per predictor,
#'   and model formula(s) as string.
#'   Used as input to computed columns in Cornerstone (e.g. to create
#'   predicted-response graph).
#'   }
#'   \item{rgobjects}{
#'   List of \code{glm} objects with fitted Logistic Regression.
#'   }
#' @seealso \code{\link[stats]{glm}}
#' @export
#' @examples
#' # simulate data
#' x1 <- rnorm(1000)   # some continuous variables
#' x2 <- rnorm(1000)
#' x3 <- sample(letters[1:3], 1000, replace = TRUE) # some factor variable
#' pr <- 1/(1 + exp(-(1 + 2*x1 + 3*x2)))
#' y <- rbinom(1000, 1, pr)
#' data <- data.frame(x1, x2, x3, y, stringsAsFactors = TRUE)
#' # define response variable
#' resps <- "y"
#' # define predictors
#' preds <- c("x1", "x2", "x3")
#' # example settings for script variables
#' scriptvars <- list(use.rows = "all", link = "logit",
#'                   preds.fmla = "Linear",
#'                   significance.level = "0.05",
#'                   stepwise = FALSE)
#' # run function
#' res <- logisticRegression(dataset = data, scriptvars = scriptvars,
#' preds = preds, resps = resps, brush = rep(FALSE, nrow(data)), 
#' return.results = TRUE)
logisticRegression <- function(dataset = cs.in.dataset(),
                               scriptvars = cs.in.scriptvars(),
                               preds = cs.in.predictors(),
                               resps = cs.in.responses(),
                               brush = cs.in.brushed(),
                               return.results = FALSE) {
  # input as data table
  dtDataset <- as.data.table(dataset)

  # check inputs
  assertDataTable(dtDataset, min.rows = 1, min.cols = 1)
  if (any(is.na(dtDataset))) {
    warning("The missing values will be omitted.")
  } # Cornerstone does not display any warnings

  assertCharacter(preds)
  assertCharacter(resps, min.len = 1, all.missing = FALSE)
  assertSetEqual(names(dtDataset), c(preds, resps))
  assertDisjunct(names(dtDataset), c("pred", "preds", "resp", "resps", "brush", 
                                     "brushed"))
  assertDataTable(dtDataset[, preds, with = FALSE], all.missing = FALSE)
  assertDataTable(dtDataset[, resps, with = FALSE], all.missing = FALSE)
  dtDataset[, resps] <- lapply(dtDataset[, resps, with = FALSE], as.factor)
  assertFactor(unlist(dtDataset[, resps, with = FALSE]), n.levels = 2)
  
  assertList(scriptvars, len = 5)
  assertChoice(scriptvars$use.rows, c("all", "non-brushed", "brushed"))
  assertChoice(scriptvars$link, c("logit", "probit"))
  assertChoice(scriptvars$preds.fmla,
               c("Linear", "Linear+", "Interactions", "Quadratic"))
  assertChoice(scriptvars$significance.level,
               c("0.10", "0.05", "0.01", "0.001"))
  assertFlag(scriptvars$stepwise)
  assertFlag(return.results)

  # update to valid names
  orig_preds <- preds <- make.names(preds)
  resps <- make.names(resps)
  names(dtDataset) <- make.names(names(dtDataset))
  
  # global variables
  correlation <- Variable1 <- Variable2 <- x <- y <- NULL
  ndata <- nrow(dtDataset)
  
  # subsetting data via brush
  if (scriptvars$use.rows == "all") {
    brush[] <- TRUE
  }
  if (scriptvars$use.rows == "non-brushed") {
    brush <- !brush
  }

  # retrieve numerical / categorical predictors
  num_preds <- unlist(lapply(dtDataset, is.numeric))
  cat_preds <- unlist(lapply(dtDataset, function(x) (is.factor(x) |
                                                       is.character(x))))
  orig_num_preds <- num_preds <- names(dtDataset[, num_preds, with = FALSE])
  orig_cat_preds <- cat_preds <- names(dtDataset[, cat_preds, with = FALSE])[
    names(dtDataset[, cat_preds, with = FALSE]) %in% preds]
  
  # make sure that cat_preds all of type factor
  if (length(cat_preds))
    dtDataset <- dtDataset[, (cat_preds) := lapply(.SD, as.factor), 
                           .SDcols = cat_preds]
  
  ### init resulting output tables
  # regression table + evaluation measures
  tab_regr_complete <- data.frame(Terms = character())
  tab_regr_eval_complete <- data.frame(Terms = c("R-Square", "Adj R-Square",
                                                 "RMS Error",  "Residual df"), 
                                       stringsAsFactors = FALSE) 
  table_gof_complete <- data.frame()
  table_coeff_list <- list()
  table_cormatrix_list <- list()

  # Fit Estimate table
  table_fitted_complete <- data.table(logical(ndata))
  names(table_fitted_complete) <- paste(c("V", resps), collapse = "")
  for (resp in resps) {
    table_fitted_complete[, (paste0("Used.", resp)) :=
             as.integer(!apply(dtDataset[, c(preds, resp), with = FALSE], 
                              1, function(x) any(is.na(x))) & brush)]
    table_fitted_complete[, (resp) := dtDataset[, resp, with = FALSE]]
    table_fitted_complete[, (paste0("Pred.", resp)) := numeric(ndata)]
    table_fitted_complete[, (paste0("Resid.", resp)) := numeric(ndata)]
    table_fitted_complete[, (paste0("Leverages.", resp)) := NA_real_]
  }
  table_fitted_complete[, (paste(c("V", resps), collapse = "")) := NULL]
  
  # Predicted response table
  pred_table_list <- list()
  
  # Model Fit (output as R object)
  mod_list <- list()
  
  # failed convergence table
  resp_converged <- c(!logical(length(resps)))
  names(resp_converged) <- resps
  not_converged <- data.table()

  ############### fit logistic regression #################
  for (resp in resps) {
    # restore preds to its original input values
    preds <- orig_preds
    cat_preds <- orig_cat_preds
    num_preds <- orig_num_preds
    # nullmodel (for comparison)
    fmla_null <- as.formula(paste(resp, " ~ 1"))
    mod_null <- glm(fmla_null, data = dtDataset[brush],
                  family = binomial(link = scriptvars$link))
    # build formula
    if (!length(preds)) {
      fmla <- fmla_null
      mod <- mod_null
    } else {
      # basic linear formula
      lin <- paste0(resp, " ~ (", paste(preds, collapse = "+"), ")")
      fmla <- switch(scriptvars$preds.fmla,
                 #"Linear-" not implemented
                 "Linear" = lin,
                 "Linear+" = paste(lin, "+", 
                                   paste(paste0("I(", num_preds, "^2)"),
                                         collapse = "+")),
                 "Interactions" = paste0(lin, "^", length(preds)),
                 "Quadratic" = paste(lin, "^", length(preds), "+",
                                     paste(paste0("I(", num_preds, "^2)"),
                                           collapse = "+")))
      if (!length(num_preds)) {
        fmla <- switch(scriptvars$preds.fmla,
                   "Linear" = lin,
                   "Linear+" = lin,
                   "Interactions" = paste0(lin, "^", length(preds)),
                   "Quadratic" = paste0(lin, "^", length(preds)))
      }
      # final model
      options(contrasts = c("contr.sum", "contr.sum")) # unordered and ordered
      mod <- glm(as.formula(paste(fmla)), 
               data = dtDataset[brush, c(resp, preds), with = FALSE],
               family = binomial(scriptvars$link))
      if (requireNamespace("car")) {
        # for p values of stepwise regression
        mod_anova <- car::Anova(mod, test = "F")
      }
    }
    
    if (any(is.na(coef(mod)))) { # happens with singularities
      na_coef <- names(which(is.na(coef(mod))))
      preds <- preds[preds != na_coef]
    }

    # create regression table
    p_values <- summary(mod)$coefficients[, 4]
    # McFadden's R squared for logistic Regression
    pseudo_R2 <- 1 - logLik(mod)[1] / logLik(mod_null)[1]
    pseudo_adj_R2 <- 1 - (logLik(mod)[1] / logLik(mod_null)[1]) *
      (mod$df.null / mod$df.residual)
    rms_error <- sqrt((-2 * logLik(mod)[1]) / mod$df.residual)
    if (is.null(names(p_values))) names(p_values) <- "(Intercept)"
    tab_regr <- data.frame(Terms = names(p_values), Significance = p_values)
    row.names(tab_regr) <- seq_len(nrow(tab_regr))

    # evaluation measures (will be added in the end to tab_regr)
    tab_regr_eval <- data.frame(Significance = c(pseudo_R2, pseudo_adj_R2, 
                                                 rms_error, mod$df.residual))

    # stepwise regression based on significance
    if (scriptvars$stepwise && length(preds)) {
      # initial degree to check by function
      degree <- max(attr(summary(mod)$terms, "order"))
      termlabels <- attr(mod$terms, "term.labels")
      if (degree == 1) { # change degree to 2 for quadratic terms
        int <- termlabels[
            stringr::str_detect(termlabels, stringr::fixed("I(")) &
            stringr::str_detect(termlabels, stringr::fixed("^2)"))]
        if (length(int)) degree <- 2
      }

      while (degree > 0) { # intercept to be kept
        del <- step_significance(mod, mod_anova, degree = degree,
                              cat_preds = cat_preds,
                              alpha = as.numeric(scriptvars$significance.level))
        if (length(del)) {
          del_ind <- unlist(lapply(del, function(l) {
            grep(l, attr(mod$terms, "term.labels"), fixed = TRUE)
            }))
          if (length(del_ind) == length(attr(mod$terms, "term.labels"))) {
            mod <- mod_null # no significance
          } else {
            mod <- update(mod,
                          formula = drop.terms(mod$terms, unique(del_ind),
                                             keep.response = TRUE))
            if (requireNamespace("car")) {
              # for p values of stepwise regression
              mod_anova <- car::Anova(mod, test = "F")
            }
          }
        }
        if (!length(attr(mod$terms, "term.labels"))) {
          degree <- 0
          break
        }
        # check significance
        step <- step_significance(mod, mod_anova, degree, cat_preds, alpha =
                                  as.numeric(scriptvars$significance.level),
                                  only.check = TRUE)
        if (any(!step$sig, na.rm = TRUE)) {
          next
        }
        degree <- degree - 1
      }

      # update preds
      preds <- preds[preds %in% attr(summary(mod)$terms, "term.labels")]
      num_preds <- num_preds[num_preds %in% attr(summary(mod)$terms,
                                                "term.labels")]
      cat_preds <- cat_preds[cat_preds %in% attr(summary(mod)$terms,
                                                "term.labels")]

      # re-create regression table after stepwise regression
      p_values <- summary(mod)$coefficients[, 4]
      pseudo_R2 <- 1 - logLik(mod)[1] / logLik(mod_null)[1]
      pseudo_adj_R2 <- 1 - (logLik(mod)[1] / logLik(mod_null)[1]) *
        (mod$df.null / mod$df.residual)
      rms_error <- sqrt((-2 * logLik(mod)[1]) / mod$df.residual)

      if (is.null(names(p_values))) names(p_values) <- "Constant"

      tab_regr <- data.frame(Terms = names(p_values), Significance = p_values)
      row.names(tab_regr) <- seq_len(nrow(tab_regr))

      # evaluation measures (will be added in the end to tab_regr)
      tab_regr_eval <- data.frame(Significance = c(pseudo_R2, pseudo_adj_R2, 
                                                   rms_error, mod$df.residual))
    }
    if (!mod$converged) { #"Model algorithm did not converge"
      resp_converged[which(resps == resp)] <- FALSE 
      next 
    }

    ############### outputs #################################
    # 1. goodness of fit
    table_gof <- data.table(Response = resp,
                            Count = nrow(dtDataset),
                            R_Square = pseudo_R2,
                            Adj_R_Square = pseudo_adj_R2,
                            RMS_Error = rms_error)
    names(table_gof) <- c("Response", "Count", "R-Square", "adj R-Square",
                          "RMS Error")


    # 2. coefficient table
    tab_coeff <- as.data.table(cbind(summary(mod)$coefficients,
                                exp(summary(mod)$coefficients[, 1])),
                               keep.rownames = TRUE)
    names(tab_coeff) <- c("Term", "Coefficient", "Std Error", "t-Value",
                          "Significance", "Exp Coefficient")
    table_coeff <- tab_coeff[, c(1:2, 6, 3:5)]


    # 3. coefficient correlation matrix
    tab_cormatrix <- as.data.frame(cov2cor(summary(mod)$cov.scaled))
    table_cormatrix <- as.data.frame(cbind(
      Name = names(tab_cormatrix), tab_cormatrix))
    table_cormatrix$Name <- as.character(table_cormatrix$Name)
    rownames(table_cormatrix) <- seq_len(nrow(table_cormatrix))


    # 4. fitted values
    if (length(preds)) { # may fail with multicollinearity
      pred_vals <- try(round(stats::predict(mod, dtDataset[, preds, with = FALSE], 
                                type = "response")))
    } else {
      pred_vals <- try(round(stats::predict(mod, type = "response")))
    }
    table_fitted_complete[, (paste0("Pred.", resp)) := pred_vals]
    table_fitted_complete[, (paste0("Resid.", resp)) := # residuals(mod, type = "response")
                      as.integer(table_fitted_complete[, resp, with = FALSE] !=
                      table_fitted_complete[, paste0("Pred.", resp), 
                                            with = FALSE])]
    used_resp <- which(!apply(dtDataset[, c(preds, resp), with = FALSE], 
                            1, function(x) any(is.na(x))) & brush)
    table_fitted_complete[used_resp, 
                          (paste0("Leverages.", resp)) := influence(mod)$hat]


    ### adapt tables to CS Layout
    tab_regr[1, 1] <- constant_repl(tab_regr[1, 1])
    table_coeff$Term[1] <- constant_repl(table_coeff$Term[1])
    names(table_cormatrix)[2] <- constant_repl(names(table_cormatrix)[2])
    table_cormatrix[1, 1] <- constant_repl(table_cormatrix[1, 1])

    tab_regr[, 1] <- str_repl(tab_regr[, 1])
    table_coeff$Term <- str_repl(table_coeff$Term)
    names(table_cormatrix) <- str_repl(names(table_cormatrix))
    table_cormatrix[, 1] <- str_repl(table_cormatrix[, 1])

    # change table layout for categorical predictors
    if (length(cat_preds)) {
      table_coeff_temp <- layout_preds_table(as.data.frame(table_coeff),
                                           cat_preds, dtDataset, FALSE)
      # NA warning here is "normal", comes from rbind() in layout_preds_table()
      # correct columns 3:6 for coefficient table
      for (i in cat_preds) {
        lvls <- levels(unlist(dtDataset[, i, with = FALSE]))
        ref_cats <- lvls[length(lvls)]
        table_coeff_temp[table_coeff_temp$Term %in% ref_cats, 3] <- exp(
          table_coeff_temp[table_coeff_temp$Term %in% ref_cats, ]$Coefficient)
        table_coeff_temp[table_coeff_temp$Term %in% ref_cats, 4:6] <- NA
      }
      table_coeff <- table_coeff_temp
      table_cormatrix <- layout_corrmatrix(table_cormatrix, cat_preds, dtDataset)
    }

    ###########################################################################
    # 5. predictions table with softmax/pnorm formula
    if (!length(preds)) {
      pred_table <- data.table("Formula" = linkfct(tab_coeff$Coefficient,
                                                   scriptvars$link))
      names(pred_table) <- resp
    } else {
      # create table with the part in the formula for each coefficient
      coeff_tab <- tab_coeff[, 1:2]
      coeff_tab[, 1] <- table_cormatrix[, 1]
      coeff_tab <- as.data.frame(coeff_tab)
      if (length(num_preds)) {
        for (num in num_preds) {
          coeff_tab$Term <- stringr::str_replace_all(coeff_tab$Term,
                                                   num, paste0("\"", num, "\""))
        }
      }
      coeff_tab$fmla_part <- paste(coeff_tab[, 1], paste0("(", coeff_tab[, 2],
                                                          ")"), sep = " * ")
      # create string vector for the extraValues Formula extraVal_string,
      # update coeff_tab formulas
      extraValText_full <- c()
      if (length(cat_preds)) {
        for (cat in cat_preds) {
          lvls <- levels(unlist(dtDataset[, cat, with = FALSE]))
          extraValText_string <- c()
          i <- 0
          replace_expr <- vector("list", length(lvls) - 1)
          names(replace_expr) <- lvls[1:length(lvls) - 1]
          for (lvl in lvls) {
            i <- i + 1
            search_expr <- paste0(cat, lvl)
            index <- sapply(search_expr, function(y) grep(y, coeff_tab$fmla_part))
            if (length(unlist(index))) {
              replace_expr[[lvl]] <- character(length(unlist(index)))
              names(replace_expr[[lvl]]) <- index
              for (ii in unlist(index)) {
              replace_expr[[lvl]][as.character(ii)] <- stringr::str_replace_all(
                coeff_tab$fmla_part[ii], stringr::fixed(
                  paste0(search_expr, " * ")), "")
              coeff_tab$fmla_part[ii] <- paste0(
                "if(\"", cat, "\" = '", lvl, "') then(", replace_expr[[lvl]][as.character(ii)],
                ") else(0)")
              }
              last_index <- index
              last_se <- search_expr
            } else {  # add ref cat
              j <- 0
              for (li in last_index) {
                coeff_tab <- rbind(coeff_tab[1:(li + j), ], 
                                  ref = NA, 
                                  coeff_tab[-(1:(li + j)), ])
                ii <- li + j + 1 # index of ref
                row.names(coeff_tab) <- 1:nrow(coeff_tab)
                coeff_tab$Term[ii] <- stringr::str_replace_all(
                  coeff_tab$Term[li + j], last_se, search_expr)
                j <- j + 1
                coeff_tab$fmla_part[ii] <- paste0(
                  "if(\"", cat, "\" = '", lvl, "') then(-(", 
                  paste(unlist(lapply(replace_expr, function(x) return(x[j]))), 
                  collapse = "+"),")) else(0)")
              }
            }
            extraValText_string <- c(extraValText_string, 
                                     paste0("if(RowNumber=", i, ") then('", lvl,
                                        "') else("))
          }
          extraValText_full <- c(extraValText_full, paste0(paste0(
            extraValText_string, collapse = ""), "[MISSING]",
            paste0(rep(")", length(lvls)), collapse = "")))
        }
      }
      coeff_tab$fmla_part <- stringr::str_replace_all(coeff_tab$fmla_part,
                                                    stringr::fixed(
                                                      "Constant * "), "")
      extraVal_string <- c(rep(
        "extraVal(1)+(extraVal(2)-extraVal(1))*(RowNumber-1)/(max(RowNumber)-1)",
        length(num_preds)), extraValText_full)
      fmla_expr <- c(extraVal_string,
                   linkfct(paste0(coeff_tab$fmla_part, collapse = " + "),
                           scriptvars$link))

      # build entries for the prediction table
      rows <- data.frame(Rows = c("Min", "Max", "Mean/Mode", NA, NA, "Formula"))
      if (length(num_preds)) {
        minmax_tab <- as.data.frame(rbind(apply(dtDataset[, num_preds,
                                                        with = FALSE], 2, min,
                                              na.rm = TRUE),
                                        apply(dtDataset[, num_preds,
                                                        with = FALSE], 2, max,
                                              na.rm = TRUE),
                                        apply(dtDataset[, num_preds,
                                                      with = FALSE], 2, mean,
                                              na.rm = TRUE)))
        pred_table <- data.frame(cbind(mycbind(rows, minmax_tab), NA))
      } else {
        pred_table <- data.frame(cbind(rows, NA))
      }

      if (length(cat_preds)) {
        tab_cat <- matrix()
        mode_tab <- as.data.frame(rbind(NA, NA, apply(
          dtDataset[, cat_preds, with = FALSE], 2, Mode)))
        for (cat in cat_preds) {
          # unique sorts by appearance in data
          levels_cat <- apply(dtDataset[, cat, with = FALSE], 2,
                              function(x) sort(unique(x)))
          if (all(is.na(tab_cat))) {
            tab_cat <- as.data.frame(levels_cat)
          } else {
            tab_cat <- as.data.frame(mycbind(tab_cat, levels_cat))
          }
        }
        mode_tab <- rbind(mode_tab, NA, NA, NA, tab_cat)
        if (length(num_preds)) {
          # join entries for numerical and categorial predictors
          pred_table <- data.frame(cbind(multibind(rows, minmax_tab,
                                                     mode_tab)), NA)
        } else {
          pred_table <- data.frame(cbind(mycbind(rows, mode_tab), NA))
        }
        pred_table[, 1] <- sapply(pred_table[, 1], as.character)
        pred_table[7, 1] <- "Categories"
      }

      # add the formula(s) for the response variable from model equation
      names(pred_table)[ncol(pred_table)] <- resp
      pred_table <- sapply(pred_table, as.character)
      pred_table[6, -1] <- fmla_expr
      for (k in preds) {
        temp <- coeff_tab # take coeff_tab entries and...
        if (length(num_preds)) {
          for (kk in num_preds[num_preds != k]) {
            # ...add extraValues() where needed
            temp$fmla_part <- stringr::str_replace_all(temp$fmla_part,
                                                     paste0("\"", kk, "\""),
                                                     paste0("extraVal(3, \"",
                                                            kk, "\")"))
          }
        }
        if (length(cat_preds)) {
          for (cc in cat_preds[cat_preds != k]) {
            # ...add extraValues where needed
            temp$fmla_part <- stringr::str_replace_all(temp$fmla_part,
                                                  paste0("\"", cc, "\""),
                                                  paste0("extraValText(3, \"",
                                                         cc, "\")"))
          }
        }
        # add the correct link function (probit/logit) to the term
        fmla_expr_term <- linkfct(paste0(temp$fmla_part, collapse = " + "),
                                  scriptvars$link)
        # add entry to pred_table
        pred_table <- mycbind(pred_table, as.data.frame(c(rep(NA, 5),
                                                              fmla_expr_term)))
        names(pred_table)[ncol(pred_table)] <- paste(resp, k, sep = "_")
      }
    }
    pred_table[] <- lapply(pred_table, as.character)

    # adjust coefficient table for probit output
    if (scriptvars$link == "probit") table_coeff$`Exp Coefficient` <- NULL

    # output to CS
    cs.out.dataset(as.data.table(table_coeff), 
                   paste("Coefficient Table for", resp))
    cs.out.dataset(as.data.table(table_cormatrix), 
                   paste("Coefficient Correlation for", resp))

    ### computed prediction table (works only with CS 7.3)
    if (ncol(pred_table) == 1 && !length(preds)) {
      DF <- as.data.frame(matrix(NA, nrow = min(100, nrow(dtDataset[, resp, 
                                                               with = FALSE])),
                                     ncol = 1))
      names(DF) <- names(pred_table)
      attr(DF[, 1], "formula") <- pred_table[1][[1]]
    } else {
      DF <- as.data.frame(matrix(NA, nrow = min(100, 
                              nrow(dtDataset[, c(resp, preds), with = FALSE])),
                              ncol = ncol(pred_table) - 1))
      names(DF) <- names(pred_table)[-1]
      for (col in names(pred_table)[-1]) {
        attr(DF[, col], "extraVal1") <- pred_table[1, col]
        attr(DF[, col], "extraVal2") <- pred_table[2, col]
        attr(DF[, col], "extraVal3") <- pred_table[3, col]
        attr(DF[, col], "extraVal4") <- pred_table[4, col]
        attr(DF[, col], "extraVal5") <- pred_table[5, col]
        attr(DF[, col], "formula") <- pred_table[6, col]
      }
    }
    cs.out.dataset(as.data.table(DF), name = paste(resp, "vs. Predictors"), 
                   keep_compcol = TRUE)
    
    # Replace emf corrplot with tilemap graph
    if (length(preds)) {
    corTable <- data.table(
      x = NA, 
      y = NA, 
      correlation = table_cormatrix[, -1, drop = FALSE][
        lower.tri(table_cormatrix[, -1, drop = FALSE], diag = TRUE)],
      width = 0.8, height = 0.8,
      Variable1 = table_cormatrix$Name[row(table_cormatrix[, -1, drop = FALSE])[
        lower.tri(table_cormatrix[, -1, drop = FALSE], diag = TRUE)]],
      Variable2 = names(table_cormatrix)[-1][col(
        table_cormatrix[, -1, drop = FALSE])[
        lower.tri(table_cormatrix[, -1, drop = FALSE], diag = TRUE)]]
    )
    numRows <- sum(!is.na(corTable$correlation))
    Grp1 <- corTable[, list(numRowsGrp1 = sum(!is.na(correlation))), 
                     by = Variable1]
    Grp2 <- corTable[, list(numRowsGrp2 = sum(!is.na(correlation))), 
                     by = Variable2]
    tmpColor <- merge(merge(corTable, Grp1, sort = FALSE), Grp2, sort = FALSE)
    corTable[, x := as.integer(sqrt(0.25 + 2 * numRows) + 1.5 - 
                                 tmpColor$numRowsGrp1)]
    corTable[, y := as.integer(0.5 + sqrt(0.25 + 2 * numRows) + 1 - 
                                 tmpColor$numRowsGrp2)]
    cs.out.graph(corTable[, -(6:7)], 
                 name = paste("Coefficient Correlation Heatmap", resp), 
                 graphtype = "TileMap") #works but data labeling problem remains
    }
    
    # append output tables
    if (length(resps) > 1) {
      names(tab_regr)[2] <- colnames(tab_regr_eval) <- paste("Significance", 
                                                             resp, sep = "_")
    }
    tab_regr_complete <- merge(tab_regr_complete, tab_regr, 
                                       by = "Terms", all = TRUE, sort = FALSE)
    tab_regr_eval_complete <- cbind(tab_regr_eval_complete, tab_regr_eval)
    table_gof_complete <- as.data.table(rbind(table_gof_complete, table_gof))
    table_coeff_list[[resp]] <- as.data.table(table_coeff)
    table_cormatrix_list[[resp]] <- as.data.table(table_cormatrix)
    pred_table_list[[resp]] <- as.data.table(pred_table)
    mod_list[[resp]] <- mod
  }
  
  # output models that did not converge
  if (!all(resp_converged)) {
    not_converged <- data.table(
      Response = names(resp_converged[!resp_converged]), 
      Info = "Model did not converge")
    cs.out.dataset(not_converged, "Failed Model Fits", brush = FALSE)
  }
  
  # output models that did converge
  if (any(resp_converged)) {
    # correct layout regression table
    if (length(orig_cat_preds)) {
      tab_regr_complete <- layout_preds_table(tab_regr_complete, orig_cat_preds, 
                                            dtDataset, TRUE)
    }
    # join regression table with evaluation measures
    tab_regr_complete <- rbind(tab_regr_complete, tab_regr_eval_complete)
    tab_regr_complete[, -1] <- sapply(tab_regr_complete[, -1], as.numeric)

    # unlist if there is only one resp
    if (length(resps) == 1) {
      table_coeff_list <- table_coeff_list[[1]]
      table_cormatrix_list <- table_cormatrix_list[[1]]
      pred_table_list <- pred_table_list[[1]]
    }
  
    for (col in names(table_fitted_complete)) 
      attr(table_fitted_complete[[col]], "formula") <- NULL
  
    # output to CS
    cs.out.dataset(table_gof_complete, "Goodness of Fit", brush = FALSE)
    cs.out.dataset(table_fitted_complete, "Fit Estimate", brush = TRUE)
    cs.out.dataset(as.data.table(tab_regr_complete), "Regression Dataset", 
                 brush = FALSE)
    cs.out.Robject(mod_list, "Logistic Regression Models")
  }
  
  # return results
  if (return.results) {
    res <- list(failed_convergence = not_converged,
                regression_table = as.data.table(tab_regr_complete),
                goodness_of_fit = table_gof_complete,
                coeff_table = table_coeff_list,
                coeff_cor_matrix = table_cormatrix_list,
                fitted_values = table_fitted_complete,
                prediction_table = pred_table_list,
                rgobjects = list(mod_list)
                #, cor_table = corTable
    )
    return(res)
  } else {
    invisible(TRUE)
  }
}


Mode <- function(x) {
  # calculate Mode of a variable
  ux <- unique(x)
  return(ux[which.max(tabulate(match(x, ux)))])
}


linkfct <- function(fmla, link) {
  # output string represents calculation of probability that Y = 1 (Y binary)
  # (so-called softmax function for logit link and pnorm for probit link)
  if (link == "logit") link_string <- paste0("1 / (1 + exp(-(", fmla, ")))")
  if (link == "probit") link_string <- paste0("pnorm(", fmla, ")")
  return(link_string)
}


step_significance <- function(mod, mod_anova, degree, cat_preds, alpha,
                              only.check = FALSE) {
  # output next step of stepwise regression
  #
  # mod: full/last regression model
  # mod_anova: anova of model (needed for categorical variables)
  # degree: highest degree of model
  # alpha: significance level (default is 0.05)
  # only.check: only check if there is a next step (boolean)
  p_vals <- data.frame(p_vals = summary(mod)$coefficients[, 4])
  order <- data.frame(order = attr(summary(mod)$terms, "order"))
  rownames(order) <- attr(summary(mod)$terms, "term.labels")

  table <- merge(p_vals, order, by = 0, all = TRUE)
  names(table)[1] <- "coeffs"
  table[table$coeffs == "(Intercept)", ]$order <- 0

  int <- table[stringr::str_detect(table$coeff, stringr::fixed("I(")) &
                 stringr::str_detect(table$coeff,
                                     stringr::fixed("^2)")), ]$order
  if (length(int)) {
    table[stringr::str_detect(table$coeff, stringr::fixed("I(")) &
            stringr::str_detect(table$coeff,
                                stringr::fixed("^2)")), ]$order <- 2
  }

  table[is.na(table$order), ]$order <- stringr::str_count(
    table[is.na(table$order), ]$coeffs, ":") + 1

  table[is.na(table$p_vals), ]$p_vals <- mod_anova[table[
    is.na(table$p_vals), ]$coeffs, "Pr(>F)"]
  table <- table[table$coeffs %in% c(rownames(mod_anova), "(Intercept)"), ]

  table <- table[order(table$order, table$p_vals, table$coeffs), ]
  rownames(table) <- seq_len(nrow(table))
  table$sig <- TRUE
  step <- table[table$order == degree, ]
  step$sig <- step$p_vals <= alpha

  # save linear predictors for model hierarchy
  lin_preds <- table[table$order == 1, ]$coeffs

  if (any(!step$sig, na.rm = TRUE)) {
    # check for higher levels (model hierarchy)
    # --> turn true if pred available on higher level
    if (degree != max(table$order)) {
      lin_index_higher <- sapply(lin_preds, function(r) grep(r, table[
        table$order == degree + 1, ]$coeffs))
      lin_index <- sapply(lin_preds, function(r) grep(r, table[
        table$order == degree, ]$coeffs))
      lin_index_higher <- lin_index_higher[lapply(lin_index_higher, length) > 0]
      lin_index <- lin_index[lapply(lin_index, length) > 0]

      if (length(names(lin_index) %in% names(lin_index_higher))) {
        row.names(step) <- seq_len(nrow(step))
        step[unlist(lin_index[names(lin_index) %in%
                                names(lin_index_higher)]), ]$sig <- TRUE
      }
    }
    if (only.check) return(step)
    return(step[!step$sig & !is.na(step$sig), ]$coeffs)
  }
  if (only.check) return(step)
  return(character(0))
}


layout_preds_table <- function(tab, cat_preds, dtDataset, regtab = TRUE) {
  # output correct layout for table with predictors
  #
  # tab: a data.frame table
  # cat_preds: vector of categorical predictors
  # dtDataset: original input dtDataset as data.table
  row.names(tab) <- seq_len(nrow(tab))
  list_ind_full <- list()
  list_index_full <- list()

  for (i in cat_preds) {
    cat_levels <- levels(unlist(dtDataset[, i, with = FALSE]))
    n <- length(cat_levels) - 1 # number of groups appearing in tables
    x <- tab[, 1]
    index <- sapply(i, function(y) grep(y, x))
    if (length(unlist(index))) {
      list_ind <- split(x[index], ceiling(seq_along(x[index]) / n))
      list_index <- lapply(list_ind, function(r) {
        chars <- as.character(r)
        chars1 <- c(substr(chars[1], 1, nchar(chars[1])), cat_levels)
        return(stringr::str_replace(chars1, paste0(i, "1"), i))
      })
      list_index_full <- append(list_index_full, list_index)
      list_ind <- lapply(list_ind, function(l) which(x %in% l))
      names(list_ind) <- unlist(lapply(list_index, `[[`, 1))
      list_ind_full <- append(list_ind_full, list_ind)
    }
  }
  if (length(list_ind_full)) {
    temp <- tab[1:(list_ind_full[[1]][1] - 1), ] # start_tab

    for (entry in seq_len(length(list_index_full))) {
      part_entry <- list_index_full[[entry]]
      part_entry2 <- list_ind_full[[entry]]
      if (entry != 1) {
        mid <- list_ind_full[[entry - 1]][length(list_ind_full[[entry - 1]])] + 1
        if (mid != part_entry2[1]) {
          middle <- tab[mid:(part_entry2[1] - 1), ]
          temp <- rbind(temp, middle)
        }
      }
      temp_part_table <- cbind(part_entry[2:(length(part_entry) - 1)],
                             tab[part_entry2, -1])
      if (regtab) {
        part_table <- as.data.frame(rbind(
          NA,
          temp_part_table,
          NA
        ))
      }
      else {
        part_table <- as.data.frame(rbind(
          NA,
          temp_part_table,
          tab[1, 2] - sum(temp_part_table[, 2])
        ))
      }
      part_table[, 1] <- as.character(part_table[, 1])
      part_table[1, 1] <- part_entry[1]
      part_table[nrow(part_table), 1] <- part_entry[length(part_entry)]
      names(part_table) <- names(tab)
      temp <- rbind(temp, part_table)
      row.names(temp) <- seq_len(nrow(temp))
    }
    ending <- list_ind_full[[length(list_ind_full)]][length(list_ind_full[[
      length(list_ind_full)]])]
    if (ending != nrow(tab)) {
      end_tab <- rbind(temp, tab[(ending + 1):nrow(tab), ])
      row.names(end_tab) <- seq_len(nrow(end_tab))
      return(end_tab)
    }
    return(temp)
  } else {
      return(tab)
    }
}


layout_corrmatrix <- function(tab, cat_preds, dtDataset) {
  # return correct layout for correlation matrix
  # similar to layout_preds_table above (same args)
  list_ind_full <- list()
  list_index_full <- list()
  for (i in cat_preds) {
    cat_levels <- levels(unlist(dtDataset[, i, with = FALSE]))
    # number of groups appearing in tables (without reference category)
    n <- length(cat_levels) - 1
    x <- tab[, 1]
    index <- sapply(i, function(y) grep(y, x))
    list_ind <- split(x[index], ceiling(seq_along(x[index]) / n))
    list_index <- lapply(list_ind, function(r) {
      chars <- as.character(r)
      return(stringr::str_replace(chars, paste0(i, 1:n),
                                  paste0(i, cat_levels[-(n + 1)])))
    })
    list_index_full <- append(list_index_full, list_index)
    list_ind <- lapply(list_ind, function(l) which(x %in% l))
    names(list_ind) <- unlist(lapply(list_index, `[[`, 1))
    list_ind_full <- append(list_ind_full, list_ind)
  }
  for (entry in seq_len(length(list_index_full))) {
    part_entry <- list_index_full[[entry]]
    part_entry2 <- list_ind_full[[entry]]
    x[part_entry2] <- part_entry
  }
  tab[, 1] <- x
  names(tab)[-1] <- x
  return(tab)
}


constant_repl <- function(string) {
  # replace "(Intercept)" with "Constant"
  if (string == "(Intercept)") string <- "Constant"
  return(string)
}

str_repl <- function(col) {
  # replace ":" with " * "
  col <- stringr::str_replace_all(col, ":", " * ")
  # replace I(...^2) in quadratic terms with ...^2
  col <- stringr::str_replace_all(col, stringr::fixed("I("), "")
  col <- stringr::str_replace_all(col, stringr::fixed("^2)"), "^2")
  return(col)
}

mycbind <- function(d1, d2) {
  # function to cbind two data.frames with unequal number of rows
  nd1 <- nrow(d1)
  nd2 <- nrow(d2)
  if (nd1 == nd2) 
    return(cbind(d1, d2))
  
  if (nd1 < nd2) {
    for (i in (nd1 + 1):nd2)
      d1 <- rbind(d1, NA)
  } else {
    for (i in (nd2 + 1):nd1)
      d2 <- rbind(d2, NA)
  }
  return(cbind(d1, d2))
}

multibind <- function(...) {
  # mycbind for multiple data frames
  # input: data.frames
  input <- list(...)
  n <- length(input)
  d1 <- input[[1]]
  if (n < 2) return(d1)
  for (i in 2:n) {
    d2 <- input[[i]]
    d1 <- mycbind(d1, d2)
  }
  return(d1)
}
