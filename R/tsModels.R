#' @title Time Series Models
#' @description
#' Fit common Time Series Models from ARIMA family using 
#' \code{\link[forecast]{auto.arima}} for time series forecasting.
#' @template dataset
#' @template predictors
#' @template responses
#' @template groups
#' @template scriptvars
#' @template returnResults
#' @templateVar packagelink \code{\link[forecast]{auto.arima}}
#' @export
#' @details
#' The time stamp column should be assigned to predictors and the 
#' time series columns are the responses.
#' The function fits an ARIMA (Autoregression Integrated Moving Average) with parameters 
#' p, d, q model to time series data. The parameters p, d and q are estimated 
#' using \code{\link[forecast]{auto.arima}} within the range given with the script variables. \cr
#'   Four script variables are summarized in \code{scriptvars} list:\cr
#'   \describe{
#'     \item{pMin}{[\code{integer(1)}]\cr
#'       The minimum number of lag observations included in the model, also called the 
#'       lag order. Default is 0.}
#'     \item{pMax}{[\code{integer(1)}]\cr
#'       The maximum number of lag observations included in the model, also called the 
#'       lag order. Default is 5.}
#'     \item{dMax}{[\code{integer(1)}]\cr
#'       The maximum number of times that the raw observations are differenced, also 
#'       called the degree of differencing. Default is 2.}
#'     \item{qMin}{[\code{integer(1)}]\cr
#'       The minimum size of the moving average window, also called the order of moving 
#'       average. Default is 0.}
#'     \item{qMax}{[\code{integer(1)}]\cr
#'       The maximum size of the moving average window, also called the order of moving 
#'       average. Default is 5.}
#'     \item{forecasts}{[\code{integer(1)}]\cr
#'       The number of forecasts based on the fitted time series model. Default
#'       is 5.}
#'       }
#' @return
#'   Logical [\code{TRUE}] invisibly and outputs to Cornerstone or,
#'   if \code{return.results = TRUE}, \code{\link{list}} of
#'   resulting \code{\link{list}} objects:
#'   \item{fitEst}{
#'   Fit Estimate table. Contains used, true and fitted values with residuals.
#'   }
#'   \item{GoF}{
#'   Goodness of Fit table. Contains response variable, count, degrees of 
#'   freedom, log-likelihood, AIC, AICc, BIC, R-Squared, adjusted R-Squared, 
#'   RMS error and run time in seconds.
#'   }
#' @examples
#' sim_data <- data.frame(times = seq_len(100), 
#' x = arima.sim(list(order = c(1,1,0), ar = 0.7), n = 99))
#' tsModels(sim_data, 
#'   preds = "times", 
#'   resps = "x", 
#'   scriptvars = list(pMin=0,
#'     pMax=5,
#'     dMax=3,
#'     qMin=0,
#'     qMax=5,
#'     forecasts=10),
#'   return.results = TRUE)

tsModels <- function(dataset = cs.in.dataset(),
                     preds = cs.in.predictors(),
                     resps = cs.in.responses(),
                     groups = cs.in.groupvars(),
                     scriptvars = cs.in.scriptvars(),
                     return.results = FALSE) {
   dtDataset <- as.data.table(dataset)

  # check inputs
  assertDataTable(dtDataset, min.rows = 2, min.cols = 1, all.missing = FALSE, 
                  types = c("numeric","POSIXct","POSIXt", "character", "factor"))
  assertCharacter(preds, max.len = 1, any.missing = FALSE) # time stamps
  assertCharacter(resps, min.len = 1, any.missing = FALSE) # multiple responses variables
  assertSetEqual(names(dtDataset), c(preds, resps, groups))
  assertDisjunct(names(dtDataset), c("pred", "preds", "resp", "resps"))
  assertDataTable(dtDataset[, preds, with = FALSE], all.missing = FALSE)
  assertDataTable(dtDataset[, resps, with = FALSE], all.missing = FALSE)
  assertList(scriptvars, len = 6)
  assertCount(scriptvars$pMin)
  assertCount(scriptvars$pMax)
  assertCount(scriptvars$dMax)
  assertCount(scriptvars$qMin)
  assertCount(scriptvars$qMax)
  assertCount(scriptvars$forecasts)
  assertFlag(return.results)
  
  if (length(groups) == 0) {
    groups <- tail(make.unique(c(resps, "grps")),1)
    dtDataset[, (groups) := "A"]
  } else {
    assertCharacter(groups, max.len = 1, any.missing = FALSE)
    dtDataset[,groups] <- lapply(dtDataset[,groups,with=FALSE], as.character)
  }
  
  # getting the group values
  grp.vals <- unique(dtDataset[, groups, with = FALSE])
  
  # update to valid names
  preds <- make.names(preds)
  resps <- make.names(resps)
  names(dtDataset) <- make.names(names(dtDataset))
  nresps <- length(resps)
  forecasts <- as.numeric(scriptvars$forecasts)
  
  # goodness of fit (statistics table)
  gof.names <- c("Response", "Count", "Degrees of Freedom", "Log-Likelihood",
                 "AIC", "AICc", "BIC",
                 "R-Squared", "Adjusted R-Squared", "RMS Error", "Runtime (sec)")

  FitEst_list <- list()
  GoF_list <- list()
  grp_names <- c()
  
  for (i in seq_len(length(grp.vals[[1]]))) {
    
    ndata <- nrow(dtDataset[get(groups)==grp.vals[[1]][i], !groups,with=F])
    grp_names <- c(grp_names, rep(grp.vals[[1]][i], ndata+forecasts))
    
    # fit estimate + forecasts
    FitEst <- data.table(logical(ndata + forecasts))
    colnames(FitEst) <- paste(c("V", resps), collapse = "")
    for (resp in resps) {
      FitEst[, (paste0("Used.", resp)) := c(as.integer(!is.na(dtDataset[get(groups)==grp.vals[[1]][i], resp,with=F]) ),
                                            rep(0,forecasts))]
      FitEst[, (resp) := c(unlist(dtDataset[get(groups)==grp.vals[[1]][i], resp,with=F]),rep(NA,forecasts))]
      FitEst[, (paste0("Pred.", resp)) := numeric(ndata + forecasts)]
      FitEst[, (paste0("Resid.", resp)) := c(numeric(ndata),rep(NA,forecasts))]
    }
    FitEst[, (paste(c("V", resps), collapse = "")) := NULL]
    
    # goodness of fit
    GoF <- data.table(Response = resps, 
                      count = integer(nresps),
                      df = integer(nresps), 
                      llik = numeric(nresps),
                      aic = numeric(nresps),
                      aicc = numeric(nresps),
                      bic = numeric(nresps),
                      r2 = numeric(nresps),
                      adj_r2 = numeric(nresps),
                      rmse = numeric(nresps),
                      runtime = numeric(nresps))

    
    for (resp in resps) {
      
      start.time <- Sys.time()
      tsMod <- forecast::auto.arima(dtDataset[get(groups)==grp.vals[[1]][i], resp, with=F],
                                    start.p = as.numeric(scriptvars$pMin),
                                    max.p = as.numeric(scriptvars$pMax),
                                    max.d = as.numeric(scriptvars$dMax),
                                    start.q = as.numeric(scriptvars$qMin),
                                    max.q = as.numeric(scriptvars$qMax),
                                    test = "adf")
      
      FitEst[1:length(tsMod$fitted),(paste0("Pred.", resp)) := tsMod$fitted]
      FitEst[1:length(tsMod$fitted),(paste0("Resid.", resp)) := tsMod$residuals]
      
      if( forecasts > 0){ 
        foreMod <- forecast::forecast(tsMod,h=forecasts)
        FitEst[(length(tsMod$fitted)+1):(length(tsMod$fitted)+forecasts), 
               (paste0("Pred.", resp)) := foreMod$mean]
        if(length(grp.vals[[1]]) > 1){
          cs.out.png(name = paste("Forecast for", resp, grp.vals[[1]][i]), 1200, 600)
        }else{
          cs.out.png(name = paste("Forecast for", resp), 1200, 600)
        }
        plot(foreMod, xlab = preds, ylab = resp)
      }else{
        if(length(grp.vals[[1]]) > 1){
          cs.out.png(name = paste("Data ", resp, grp.vals[[1]][i]), 1200, 600)
        }else{
          cs.out.png(name = paste("Data ", resp), 1200, 600)
        }
        stats::plot.ts(tsMod$x, xlab=preds, ylab=resp)
      }
      
      order <- forecast::arimaorder(tsMod)
      df <- tsMod$nobs - order[[1]] - order[[3]] - order[[2]] - 1
      r2 <- cor(tsMod$fitted, unlist(dtDataset[get(groups)==grp.vals[[1]][i], resp, with=F]) )^2
      adj_r2 <- 1 - ((1 - r2) * ((tsMod$nobs -1)/ (tsMod$nobs -2)))
      end.time <- Sys.time()
      
      vec <- c(tsMod$nobs, df, tsMod$loglik, tsMod$aic,
               tsMod$aicc, tsMod$bic, r2[[1]], adj_r2[[1]], forecast::accuracy(tsMod)[2],
               end.time - start.time)
      set(GoF, which(resp==resps), names(GoF)[2:ncol(GoF)], as.list(vec))
    }


    for (i in names(FitEst)) attr(FitEst[[i]], "formula") <- NULL

    FitEst_list <- append(FitEst_list, list(FitEst))
    GoF_list <- append(GoF_list, list(GoF))

  }

  FitEst_final <- rbindlist(FitEst_list)
  Gof_final <- rbindlist(GoF_list)
  names(Gof_final) <- gof.names
  
  if(length(grp.vals[[1]]) > 1){
    grp_names<- as.factor(grp_names)
    FitEst_final <- cbind(Group=grp_names, FitEst_final)
    Gof_final <- cbind( Group= rep(grp.vals[[1]], each=length(resps)), Gof_final)
    names(FitEst_final)[1] <- groups
    names(Gof_final)[1] <- groups
  }
  
  cs.out.dataset(FitEst_final, "Fit Estimate")
  cs.out.dataset(Gof_final, "Goodness of Fit")
  
  if (return.results) {
    res <- list(FitEst = FitEst_final, GoF = Gof_final)
    return(res)
  } else {
    invisible(TRUE)
  }
}