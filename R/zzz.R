#' @import stats
#' @import checkmate
#' @import ggplot2
#' @import ggmosaic
#' @import data.table
#' @importFrom utils head tail
"_PACKAGE"

utils::globalVariables(c("dOther", "pOther"))
