# Cornerstone R Scripts

**CornerstoneR** package provides generic R scripts which enable you to use existing R routines in Cornerstone.

The desktop application [Cornerstone](https://www.camline.com/products/cornerstone/) is a data analysis software provided by camLine GmbH that empowers engineering teams to find solutions even faster.
The engineers incorporate intensified hands-on statistics into their projects.
They benefit from an intuitive and uniquely designed graphical Workmap concept: you design experiments (DoE) and explore data, analyze dependencies, and find answers you can act upon immediately, interactively, and without any programming.

While Cornerstone's interface to the statistical programming language [R](https://www.r-project.org/) has been available since version 6.0, the latest interface with R is even much more efficient.
Cornerstone release 8 allows you to integrate user defined R packages directly into the standard Cornerstone GUI. While CornerstoneR version 3.3.0 and 3.2.0 is only supported by Cornerstone 8, version 3.0.0 and 3.1.0 are supported by Cornerstone version 7.3, and CornerstoneR 2.0.2 is supported since version 7.1.2.1.
Your engineering team stays in Cornerstone's graphical working environment and can apply R routines immediately and without the need to deal with programming code.

Learn how to use R packages in Cornerstone on [camLineTV YouTube](https://www.youtube.com/watch?v=HEQHwq_laXU) channel (available in German).


## Documentation

1. [Overview](https://camLine.gitlab.io/CornerstoneR/docs/)
2. [Function Reference](https://camLine.gitlab.io/CornerstoneR/docs/reference/)
3. [News](https://camLine.gitlab.io/CornerstoneR/docs/news/)


## Installation

The installation of 'CornerstoneR' is written from the perspective of a 'Cornerstone' user. A 
successful installation and licensing of 'Cornerstone' is required. No experience with 'R' is
expected.

1. [Download 'R'](https://cran.r-project.org/), click on 'Download R for Windows' and 'base' on the
following page. You will then be taken to a page from which you can download the current version 
of 'R' (current CornerstoneR version supports only R version 4.1.x to 4.3.x (https://cran.r-project.org/bin/windows/base/old/)). Execute the file and follow the installation instructions.
2. Start 'R'. A window with blue text and the title 'R Console' is displayed which is embedded in
a window with the title 'RGui'. This is the default environment of 'R' where you install packages
and execute commands directly in 'R'.
3. Now install 'CornerstoneR' using one of the options from the following sections. When asking for a
download mirror you can decide freely. The first 'cloud' link automatically redirects to servers
worldwide, and is currently sponsored by 'RStudio'.
4. When packages are installed for the first time, you are asked if you want to install them into
your private library. Answer this question with Yes.


### CRAN Version

Packages at CRAN (Comprehensive R Archive Network) are precompiled and checked by the R Team. The
package 'CornerstoneR' will be updated on CRAN when new versions are released. Currently, the CornerstoneR version on CRAN is 2.0.2.

Install the version from CRAN by copying and executing the following command in the R console.
```r
install.packages("CornerstoneR")
```
Start Cornerstone and check under "File -> Preferences -> R Script" in the field "Search path of CornerstoneR package:" if the path to the CornerstoneR package is valid.

To check if everything worked correctly run "Show Versions" from following menu in a Dataset "Analyses -> CornerstoneR -> Show Versions".
It can happen that you get an error about some missing packages. To install them, simply type the following command in the R console until all missing packages are installed.
```r
install.packages("NameOfMissingPackage")
```
To update all packages from CRAN, first close 'Cornerstone' and 'R'. Afterwards open 'R' and run
the following command
```r
update.packages(ask=FALSE)
```


### Development Version

The latest features are available in the development version of 'CornerstoneR'. Your feedback is
integrated into the development process and can be tested in this version.

For the development version from GitLab we use [remotes](https://cran.r-project.org/package=remotes).
Install the development version by copying and executing the following two commands in the R console.
```r
install.packages("remotes")
remotes::install_gitlab("camLine/CornerstoneR", dependencies = TRUE, build_opts = c("--no-resave-data", "--no-manual"))
```
Sometimes it is necessary to install certain versions, for example from a specific branch.
To install a certain branch, commit, or tag append the corresponding `name` after an `@` to the repository name:
```r
remotes::install_gitlab("camLine/CornerstoneR@name", dependencies = TRUE, build_opts = c("--no-resave-data", "--no-manual"))
```

To update the development version, first close 'Cornerstone' and 'R'. Afterwards open 'R' and run
the installation command again.

### Tips and Tricks Installation
If you need to install the R package into a different folder use:
```r
install.packages("CornerstoneR", lib= "C:\\Your\\Foldername")
```
Please ensure, that CornerstoneR and all needed packages are in the same folder. You can find the folder paths by typing the following into the R console and then checking manually for package names.
```r
.libPaths()
```
The path(s) obtained are also the path you put into the "Search path of CornerstoneR package:" field of "File -> Preferences -> RScript" in Cornerstone.

### Offline Installation
In case the user has no web access, it is advised to download following needed/suggested packages and install them by hand.


| Needed Packages | Version | CRAN Link |
| :---------- | :------ | :-------- |
|checkmate| 2.3.1 | https://cran.r-project.org/web/packages/checkmate/index.html|
|data.table| 1.15.0 | https://cran.r-project.org/web/packages/data.table/index.html|
|minpack.lm| 1.2-4 | https://cran.r-project.org/web/packages/minpack.lm/index.html|
|ranger| 0.14.1 | https://cran.r-project.org/web/packages/ranger/index.html|
|rpart| 4.1.23 | https://cran.r-project.org/web/packages/rpart/index.html|
|rpart.plot| 3.1.1 | https://cran.r-project.org/web/packages/rpart.plot/index.html|
|car| 3.1-2 |  https://cran.r-project.org/web/packages/car/index.html|
|ggplot2| 3.4.4 | https://cran.r-project.org/web/packages/ggplot2/index.html|
|ggmosaic| 0.3.3 | https://cran.r-project.org/web/packages/ggmosaic/index.html|
|stringr| 1.5.1 | https://cran.r-project.org/web/packages/stringr/index.html|
|corrplot| 0.92 | https://cran.r-project.org/web/packages/corrplot/index.html|
|zoo| 1.8-12 | https://cran.r-project.org/web/packages/zoo/index.html|
|lubridate| 1.9.3 | https://cran.r-project.org/web/packages/lubridate/index.html|
|laGP| 1.5-9 | https://cran.r-project.org/web/packages/laGP/index.html|
|DoE.base| 1.2-4 | https://cran.r-project.org/web/packages/DoE.base/index.html|
|TTR| 0.24.4 | https://cran.r-project.org/web/packages/TTR/index.html|
|boot| 1.3-28.1 | https://cran.r-project.org/web/packages/boot/index.html|
|forecast| 8.21.1 | https://cran.r-project.org/web/packages/forecast/index.html|
|fitdistrplus| 1.1-11 | https://cran.r-project.org/web/packages/fitdistrplus/index.html|
|WeibullR| 1.2.1 | https://cran.r-project.org/web/packages/WeibullR/index.html|
|flexsurv| 2.2.2 | https://cran.r-project.org/web/packages/flexsurv/index.html|

| Suggested Packages | Version | CRAN Link |
| :---------- | :------ | :-------- |
|knitr| 1.45 | https://cran.r-project.org/web/packages/knitr/index.html|
|prettydoc| 0.4.1 | https://cran.r-project.org/web/packages/prettydoc/index.html|
|markdown| 1.12 | https://cran.r-project.org/web/packages/markdown/index.html|
|roxygen2| 7.2.3 | https://cran.r-project.org/web/packages/roxygen2/index.html|
|testthat| 3.2.1 | https://cran.r-project.org/web/packages/testthat/index.html|
|DiceDesign| 1.10 | https://cran.r-project.org/web/packages/DiceDesign/index.html|
